package bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public class Dbconection {

	public static Connection abrirConexion() throws Exception {
		String claseDriver = "org.mariadb.jdbc.Driver";
		String db_url = "jdbc:mysql://localhost/usuarios";
		String usuario = "root";
		String password = "usuario";

		Class.forName(claseDriver);
		Connection conn = DriverManager.getConnection(db_url, usuario, password);

		return conn;
	}

	public static void cerrarConexion(Connection conn) throws Exception {
		conn.close();
	}

	public static ResultSet getResultSet(Connection conn, String sql) throws Exception {
		
		Statement s = conn.createStatement();
		ResultSet rs = s.executeQuery(sql);
		return rs;
	}
}
