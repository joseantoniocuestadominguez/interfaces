package bbdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

public class Buscar {
	Dbconection conn = new Dbconection();


	public ResultSet buscar(String id, String nombre, String apellido1, String apellido2, String nick)
			throws Exception {
	

		String select = "SELECT id_usuario,nombre,apellido1,apellido2,nick FROM USUARIO WHERE " + "nombre LIKE ?"
				+ "OR " + "apellido1 LIKE ?" + "OR " + "apellido2 LIKE ?" + "OR " + "nick LIKE ?" + "OR "
				+ "id_usuario LIKE ?";
		Connection con = conn.abrirConexion();
		ResultSet rs = null;
		PreparedStatement ps = con.prepareStatement(select);

		ps.setString(1, nombre);
		ps.setString(2, apellido1);
		ps.setString(3, apellido2);
		ps.setString(4, nick);
		ps.setString(5, id);
		rs = ps.executeQuery();
		conn.cerrarConexion(con);
		return rs;
		

	}

}
