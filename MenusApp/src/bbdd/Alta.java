package bbdd;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

import gui.PanelAlta;

public class Alta {
	
	Dbconection conn = new Dbconection();

	public void darDeAlta(String nombre, String apellido1, String apellido2, String nick) throws Exception {

	Connection con = conn.abrirConexion();
	
	String insert = "INSERT INTO usuario (nombre, apellido1, apellido2, nick) VALUES (?,?,?,?)";
	PreparedStatement ps = con.prepareStatement(insert);
	ps.setString(1, nombre);
	ps.setString(2, apellido1);
	ps.setString(3, apellido2);
	ps.setString(4, nick);
	ps.executeUpdate();

	}

}
