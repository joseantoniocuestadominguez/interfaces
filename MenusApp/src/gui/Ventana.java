package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Image;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;

public class Ventana extends JFrame {


	private PanelAlta panelAlta = new PanelAlta();
	private PanelBuscar panelBuscar = new PanelBuscar();
	private AcercaDe acercaDe = new AcercaDe();
	private Image imagen;

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Ventana frame = new Ventana();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Ventana() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 789, 515);

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu mnNewMenu = new JMenu("Usuarios");
		mnNewMenu.setMnemonic(KeyEvent.VK_U);
		menuBar.add(mnNewMenu);

		JMenuItem mntmNewMenuItem = new JMenuItem("Alta");
		mntmNewMenuItem.setMnemonic(KeyEvent.VK_A);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				añadirPanelAlta();
			}

		});
		mnNewMenu.add(mntmNewMenuItem);

		JMenuItem mntmNewMenuItem_1 = new JMenuItem("Buscar");
		mntmNewMenuItem_1.setMnemonic(KeyEvent.VK_B);
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				añadirPanelBuscar();

			}

		});
		mnNewMenu.add(mntmNewMenuItem_1);

		JMenu mnNewMenu_1 = new JMenu("Aplicacion");
		mnNewMenu_1.setMnemonic(KeyEvent.VK_P);
		menuBar.add(mnNewMenu_1);

		JMenuItem mntmNewMenuItem_2 = new JMenuItem("Acerca de...");
		mntmNewMenuItem_2.setMnemonic(KeyEvent.VK_D);
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				JOptionPane.showMessageDialog(null, "Programa realizado por Jose Antonio Cuesta Dominguez");

			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_2);

		JMenuItem mntmNewMenuItem_3 = new JMenuItem("Salir");
		mntmNewMenuItem_3.setMnemonic(KeyEvent.VK_S);
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				salir();

			}
		});
		mnNewMenu_1.add(mntmNewMenuItem_3);
		//IMAGEN DE FONDO
		contentPane = new Fondo();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));

		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
	}

	// Añadir Paneles
	private void añadirPanelBuscar() {
		contentPane.removeAll();
		contentPane.add(panelBuscar, BorderLayout.CENTER);
		panelBuscar.setVisible(true);
		repaint();
		revalidate();
	}

	private void añadirPanelAlta() {
		contentPane.removeAll();
		contentPane.add(panelAlta, BorderLayout.CENTER);
		panelAlta.setVisible(true);
		repaint();
		revalidate();
	}

	// Salir
	private void salir() {
		int respuesta = JOptionPane.showConfirmDialog(null, "Seguro que quieres salir?", "Confirmación de salida",
				JOptionPane.YES_NO_OPTION);

		if (respuesta == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}
}
