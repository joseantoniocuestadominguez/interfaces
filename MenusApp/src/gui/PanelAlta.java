package gui;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.JTextField;

import bbdd.Alta;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Font;
import java.awt.Color;

public class PanelAlta extends JPanel {
	private JTextField textField_Nombre;
	private JTextField textField_SApellido;
	private JTextField textField_PApellido;
	private JTextField textField_Nick;
	private Alta alta = new Alta();

	/**
	 * Create the panel.
	 */
	public PanelAlta() {
		setForeground(new Color(0, 255, 255));
		setLayout(null);
		
		JLabel lblNewLabel = new JLabel("A\u00F1adir Usuario Nuevo");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(155, 11, 185, 14);
		add(lblNewLabel);
		
		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(47, 84, 49, 14);
		add(lblNombre);
		
		JLabel lblPApellido = new JLabel("Primer Apellido");
		lblPApellido.setBounds(47, 128, 108, 14);
		add(lblPApellido);
		
		JLabel lblSApellido = new JLabel("Segundo Apellido");
		lblSApellido.setBounds(48, 168, 129, 14);
		add(lblSApellido);
		
		JLabel lblNick = new JLabel("Nick");
		lblNick.setBounds(48, 203, 49, 14);
		add(lblNick);
		
		textField_Nombre = new JTextField();
		textField_Nombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				if(textField_Nombre.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				
			}
		});
		textField_Nombre.setBounds(221, 81, 96, 20);
		add(textField_Nombre);
		textField_Nombre.setColumns(10);
		
		
		
		textField_SApellido = new JTextField();
		textField_SApellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				getToolkit().beep();
				}
				if(textField_SApellido.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
			}
		});
		textField_SApellido.setBounds(221, 165, 96, 20);
		add(textField_SApellido);
		textField_SApellido.setColumns(10);
		
		textField_PApellido = new JTextField();
		textField_PApellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				getToolkit().beep();
				}
				if(textField_PApellido.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
			}
		});
		textField_PApellido.setBounds(221, 125, 96, 20);
		add(textField_PApellido);
		textField_PApellido.setColumns(10);
		
		textField_Nick = new JTextField();
		textField_Nick.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				getToolkit().beep();
				}
				if(textField_Nick.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
			}
		});
		textField_Nick.setBounds(221, 200, 96, 20);
		add(textField_Nick);
		textField_Nick.setColumns(10);
		
		JButton btnA�adir = new JButton("A\u00F1adir");
		btnA�adir.setMnemonic(KeyEvent.VK_N);
		btnA�adir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
					
				try {
					alta.darDeAlta(textField_Nombre.getText(), textField_PApellido.getText(), textField_SApellido.getText(), textField_Nick.getText());
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			
			}
		});
		btnA�adir.setBounds(142, 247, 89, 23);
		add(btnA�adir);
		
		JLabel lblNewLabel_1 = new JLabel("ATENCION!! Solo se permite letras y numeros");
		lblNewLabel_1.setBounds(47, 42, 237, 14);
		add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Longitud maxima 30 caracteres");
		lblNewLabel_2.setBounds(46, 59, 185, 14);
		add(lblNewLabel_2);

	}
}
