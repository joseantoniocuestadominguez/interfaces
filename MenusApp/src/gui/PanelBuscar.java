package gui;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.BoxLayout;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import bbdd.Buscar;

import javax.swing.JLabel;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.Font;
import java.awt.Color;

public class PanelBuscar extends JPanel {
	private JTextField textField_Nombre;
	private JTextField textField_Id_Usuario;
	private JTextField textField_Nick;
	private JTextField textField_PApellido;
	private JTextField textField_SApellido;
	private Buscar buscar = new Buscar();
	DefaultTableModel modelo;
	private ResultSet rs;

	/**
	 * Create the panel.
	 */
	public PanelBuscar() {
		setBackground(new Color(0, 255, 255));
		setForeground(new Color(0, 204, 255));
		setLayout(null);

		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(10, 155, 740, 278);
		add(scrollPane);
		// tabla
		DefaultTableModel modelo = new DefaultTableModel();
		modelo.addColumn("id_usuario");
		modelo.addColumn("nombre");
		modelo.addColumn("primer apellido");
		modelo.addColumn("segundo apellido");
		modelo.addColumn("nick");

		JTable tabla = new JTable(modelo);

		scrollPane.setViewportView(tabla);

		textField_Nombre = new JTextField();
		textField_Nombre.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				if(textField_Nombre.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				
			}
		});
		textField_Nombre.setBounds(145, 59, 96, 20);
		add(textField_Nombre);
		textField_Nombre.setColumns(10);

		JLabel lblNewLabel = new JLabel("Buscar Usuario");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 15));
		lblNewLabel.setBounds(296, 11, 148, 14);
		add(lblNewLabel);

		JLabel lblNombre = new JLabel("Nombre");
		lblNombre.setBounds(62, 62, 49, 14);
		add(lblNombre);

		JLabel lbl_Id_Usuario = new JLabel("Id Usuario");
		lbl_Id_Usuario.setBounds(62, 102, 73, 14);
		add(lbl_Id_Usuario);

		textField_Id_Usuario = new JTextField();
		textField_Id_Usuario.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				if(textField_Nombre.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				
			}
		});
		textField_Id_Usuario.setBounds(145, 99, 96, 20);
		add(textField_Id_Usuario);
		textField_Id_Usuario.setColumns(10);

		JLabel lbl_Nick = new JLabel("Nick");
		lbl_Nick.setBounds(280, 62, 49, 14);
		add(lbl_Nick);

		textField_Nick = new JTextField();
		textField_Nick.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				if(textField_Nombre.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				
			}
		});
		textField_Nick.setBounds(313, 59, 96, 20);
		add(textField_Nick);
		textField_Nick.setColumns(10);

		JLabel lbl_PApellido = new JLabel("Primer Apellido");
		lbl_PApellido.setBounds(477, 62, 114, 14);
		add(lbl_PApellido);

		textField_PApellido = new JTextField();
		textField_PApellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				if(textField_Nombre.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				
			}
		});
		textField_PApellido.setBounds(601, 59, 96, 20);
		add(textField_PApellido);
		textField_PApellido.setColumns(10);

		JLabel lbl_SApellido = new JLabel("Segundo Apellido");
		lbl_SApellido.setBounds(477, 102, 108, 14);
		add(lbl_SApellido);

		textField_SApellido = new JTextField();
		textField_SApellido.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent e) {
				char car = e.getKeyChar();
				if(Character.isLetter(car) || Character.isDigit(car)){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				if(textField_Nombre.getText().length()<31){

				}else{
				e.consume();
				//Beep es sonido de alerta
				getToolkit().beep();
				}
				
			}
		});
		textField_SApellido.setBounds(601, 99, 96, 20);
		add(textField_SApellido);
		textField_SApellido.setColumns(10);

		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.setMnemonic(KeyEvent.VK_ENTER);
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				try {
					rs = buscar.buscar(textField_Id_Usuario.getText(), textField_Nombre.getText(),
							textField_PApellido.getText(), textField_SApellido.getText(), textField_Nick.getText());
					
				} catch (Exception e1) {
					e1.printStackTrace();
				}
				

				pintarTabla(modelo);

			}
		});
		btnBuscar.setBounds(313, 98, 96, 23);
		add(btnBuscar);

	}

	private void pintarTabla(DefaultTableModel modelo) {
		modelo.setRowCount(0);
		// Se crea un array para cada fila
		Object[] fila = new Object[5]; // Hay cinco columnas en la tabla
		try {
			while (rs.next()) {

				// Se rellena cada posici�n del array las columnas
				for (int i = 0; i < 5; i++)
					fila[i] = rs.getObject(i + 1); // El primer indice en rs es el 1

				// Se a�ade al modelo la filaF
				modelo.addRow(fila);
				
			}
		} catch (SQLException e1) {
			e1.printStackTrace();

		}
	}
}
